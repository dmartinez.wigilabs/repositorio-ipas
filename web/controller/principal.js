app.controller("principal", function($scope,objetos,$state){
    $scope.opciones = objetos.getObjeto();
    $scope.url = $state.params;
    $scope.btnIphone = false
    $scope.btnNormal = true
    console.log($scope.url);
    $scope.myFunction = function(){
        document.getElementById('descargas').style.display="block";   
    }
    $scope.close =function(){
        document.getElementById('descargas').style.display="none"; 
    }
    if(BrowserDetect.browser !== 'Safari' ) {
        $scope.btnIphone = false;
        $scope.btnNormal = true
        if(navigator.appVersion.indexOf("Mac")!==-1 === true){     
            $scope.btnIphone = true;
            $scope.btnNormal = false
        }else if(navigator.appVersion.indexOf("Android")!==-1 === true){
            $scope.btnIphone = false;
            $scope.btnNormal = true
        } 
    }else{
        $scope.btnIphone = true;
        $scope.btnNormal = false;
    }   
});

