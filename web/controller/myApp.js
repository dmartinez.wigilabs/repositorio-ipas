var app = angular.module("myApp", ["ui.router","ui.bootstrap",]);
app.config(['$stateProvider','$locationProvider',function($stateProvider,$urlRouterProvider,$locationProvider) {
    
    $stateProvider
     .state('home',{
        url: "",
        templateUrl: "view/principal.html",
        controller:"principal"
    }) 
    .state('descarga',{
        url:"/descarga/:id",
        params: {
          obj : null 
        },
        templateUrl:"view/principalDescarga.html",
        controller:"principalDes"
    })
    //$urlRouterProvider.otherwise('home')
    //$locationProvider.hasPrefix('!');
}]);
app.factory("objetos", function(){
    var enlaceApp = [
        {
            "id": 1,
            "imagen": "img/claroAyuda.png",
            "titulo": "Claro te ayuda",
            "contenido":"Permite brindar ayuda en soporte, gestión de ventas  e información.",
            "imagenExp":"img/ayudaClaro.png",
            "url":""
        },
        {
            "id": 2,
            "imagen": "img/claroideas.png",
            "titulo": "Ideas que Activan",
            "contenido":"Solicita y haz seguimiento a tus pedidos de merchandising.",
            "imagenExp":"img/imagenexpli.png",
            "url":""
        },
        /*{
            "id": 3,
            "imagen": "img/soyClaro.png",
            "titulo": "Soy Claro",
            "contenido":"Accede a descuentos y cortesías de diferentes establecimientos comerciales.",
            "url":""
            
        },
        {
            "id": 4,
            "imagen": "img/claroEnlace.png",
            "titulo": "Mi enlace",
            "contenido":"Conoce el portafolio de servicios de Claro desde tu smartphone.",
            "url":""  
        },*/
    ]
    return{
         getObjeto : function(){
            return enlaceApp;
        }
    }
});